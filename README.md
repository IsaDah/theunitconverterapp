# TheUnitConverterApp

A simple app written in Java with Android Studio which allows users to convert values in the following:

1-Temperature

2-Frequency

3-Length

4-Data Rate

The app was based on this tutorial
https://androidkennel.org/unit-converter-android-tutorial/

Added more options for conversions, fixes and changes to the user interface.
